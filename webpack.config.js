function getStyleUse(bundleFilename) {
  return [
    {
      loader: 'file-loader',
      options: {
        name: bundleFilename,
      },
    },
    { loader: 'extract-loader' },
    { loader: 'css-loader' },
    {
      loader: 'sass-loader',
      options: {
        includePaths: ['../node_modules'],
      }
    },
  ];
}

module.exports = [
  {
    entry: './scss/freelancer.scss',
    output: {
      // This is necessary for webpack to compile, but we never reference this js file.
      filename: 'style-bundle-login.js',
    },
    module: {
      rules: [{
        test: /freelancer.scss$/,
        use: getStyleUse('bundle-login.css')
      }]
    },
  },
  {
    entry: "./js/main.js",
    output: {
      filename: "bundle-login.js"
    },
    module: {
      loaders: [{
        test: /main.js$/,
        loader: 'babel-loader',
        query: {presets: ['env']}
      }]
    },
  },
];